import numpy as np


def mapToFrame(map):
    def fill(frame,center_position, size, color):
        c_x,c_y = center_position
        height, widght = size
        for x in range(len(frame)):
            pixel_line = frame[x]
            for y in range(len(pixel_line)):
                if x-height//2 < c_x < x+height//2 and y-height//2 < c_y < y+height//2:
                    frame[x][y] = color
        return frame
    frame = np.zeros(shape=(500,500,3))
    for x in range(len(map)):
        line = map[x]
        for y in range(len(line)):
            tile = line[y]
            frame = fill(frame, (20*x, 20*y), (20,20),tile.color)
    print(frame)
    return frame

