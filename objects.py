import random
class tile:
    color = (0, 0, 0)
    can_walk_into = True

class Death(Exception):
    pass

class Player:
    position = (0, 0)
    can_walk_into = False
    view_field = 7
    color = (0, 255, 0)

    def __init__(self, outer_world):
        self.outer_world = outer_world

    def update_position(self):
        for x in range(len(self.outer_world)):
            line = self.outer_world[x]
            for y in range(len(line)):
                tile_to_test = line[y]
                if tile_to_test is self:
                    self.position = x, y

    def test_for_opponent(self):
        self.update_position()
        x, y = self.position
        try:
            if self.outer_world[x-1][y].attac(self):
                raise Death()
        except (IndexError, AttributeError):
            pass
        try:
            if self.outer_world[x+1][y].attac(self):
                raise Death()
        except (IndexError, AttributeError):
            pass
        try:
            if self.outer_world[x][y-1].attac(self):
                raise Death()
        except (IndexError, AttributeError):
            pass
        try:
            if self.outer_world[x][y+1].attac(self):
                raise Death()
        except (IndexError, AttributeError):
            pass

    def get_world_view(self):
        self.update_position()
        x, y = self.position

        return [
            line[
            max(0, y - self.view_field):min(len(self.outer_world), y + self.view_field)
            ]
            for line in self.outer_world[
                        max(0, x - self.view_field):min(len(self.outer_world), x + self.view_field)
                        ]
        ]

    def move(self, delta_x=0, delta_y=0):
        self.update_position()
        x, y = self.position
        if x + delta_x < 0 or y + delta_y < 0:
            return
        try:
            if self.outer_world[x + delta_x][y + delta_y].can_walk_into:
                self.outer_world[x][y] = tile()
                self.outer_world[x + delta_x][y + delta_y] = self
        except IndexError:
            pass
        self.update_position()


class opponent(Player):
    color = (100,100,100)

    def move_random(self):
        randint = random.randint(0,4)
        if randint == 0:
            self.move(-1,0)
        elif randint == 1:
            self.move(1,0)
        elif randint == 2:
            self.move(0,1)
        elif randint == 3:
            self.move(0,-1)

    def attac(self,player):
        return True

class weak_opponent(opponent):
    color = (170,50,50)
    def attac(self,player:Player):
        self.update_position()
        print(self.position)
        print(self.position)
        x, y = player.position
        if y + 1 == self.position[1]:
            self.outer_world[self.position[0]][self.position[1]] = tile()
        else:
            return True

class wall(tile):
    color = (255, 0, 0)
    can_walk_into = False

class flag(tile):
    color = (0, 100, 255)
    can_walk_into = False

    def attac(self):
        try:
            file = open('solution.txt', mode='x')
        except FileExistsError:
            file = open('solution.txt', mode='w')
        file.write('Hey du hast es gelöst, deine Flag ist 0x9079e6875885d68e2d8d96a59c7c639430f89625794 trage sie bei der Notingly minecraft webseite ein (minecraft.notingly.de/meathunt) dort dann eimpfach Insertn')
