#!/venv/bin
import time
import pygame
import map_parser
from objects import Player, tile, wall, opponent, Death

world = map_parser.load_world()
player = Player(world)
world[0][0] = player

t1 = time.time()

pygame.init()
size = (700,700)
dis = pygame.display.set_mode(size)
run = True

clock = pygame.time.Clock()


while run:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
    try:
        player.test_for_opponent()
    except Death:
        del world
        del player
        world = map_parser.load_world()
        player = Player(world)
        world[0][0] = player
    pygame.display.flip()

    keys = pygame.key.get_pressed()
    pressed_keys = []
    for key_index in range(len(keys)):
        if keys[key_index]==1:
            pressed_keys += [key_index]
    if 100 in pressed_keys: player.move(1,0)
    if 97 in pressed_keys: player.move(-1,0)
    if 115 in pressed_keys: player.move(0,1)
    if 119 in pressed_keys: player.move(0,-1)

    for x in range(len(player.get_world_view())):
        line = player.get_world_view()[x]
        for y in range(len(line)):
            rectangle = line[y]
            try:
                rectangle.move_random()
            except AttributeError:
                pass

    dis.fill((0, 0, 0))
    for x in range(len(player.get_world_view())):
        line = player.get_world_view()[x]
        for y in range(len(line)):
            rectangle = line[y]
            pygame.draw.rect(dis, rectangle.color, pygame.Rect(size[0]//2 + x*20 - int(player.view_field*20),size[1]//2 + y*20 - int(player.view_field*20), 20, 20))

    clock.tick(7)

pygame.quit()
