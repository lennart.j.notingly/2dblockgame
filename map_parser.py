import objects


def load_world():
    def toEntety(stringifield:str, world):
        if stringifield == 'wall':
            return objects.wall()
        if stringifield == 'tile':
            return objects.tile()
        if stringifield == 'FLAG':
            return objects.flag()
        if stringifield == 'oppo':
            return objects.opponent(world)
        if stringifield == 'weak':
            return objects.weak_opponent(world)
        print(stringifield)

    world = []

    with open('worldmap.txt',mode='r') as file:
        content = file.read()
        raw_lines = content.split('\n')
        all_fields = [line.split(',') for line in raw_lines]

    for line in all_fields:
        compiled_line = []
        for field in line:
            compiled_line.append(toEntety(field, world))
        world.append(compiled_line)
    return world
